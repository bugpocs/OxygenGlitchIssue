#ifndef FAKEPOST_H
#define FAKEPOST_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QTextBrowser>
#include <QIcon>
#include <QPushButton>
#include <QTimer>

#include <QDebug>

class FakePost : public QWidget
{
    Q_OBJECT

public:
    explicit FakePost(int postNumber, QWidget *parent = 0);

signals:

public slots:
    void updateContent();

    void like();
    void comment();
    void share();


private:
    QHBoxLayout *mainLayout;

    QFrame *leftFrame;

    QVBoxLayout *leftLayout;
    QLabel *avatarLabel;
    QLabel *nameLabel;

    QVBoxLayout *rightLayout;
    QTextBrowser *contentBrowser;

    QHBoxLayout *buttonsLayout;
    QPushButton *button1;
    QPushButton *button2;
    QPushButton *button3;

    QTimer *updateContentTimer;
};

#endif // FAKEPOST_H
