#-------------------------------------------------
#
# Project created by QtCreator 2016-09-19T20:39:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OxygenGlitchIssue
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    fakepost.cpp \
    faketimeline.cpp

HEADERS  += widget.h \
    fakepost.h \
    faketimeline.h
