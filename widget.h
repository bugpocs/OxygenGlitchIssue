#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTabWidget>

#include <QDebug>


#include "faketimeline.h"


class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();


private:
    QVBoxLayout *mainLayout;

    QTabWidget *tabWidget;

    FakeTimeline *tl1;
    FakeTimeline *tl2;
    FakeTimeline *tl3;
    FakeTimeline *tl4;
    FakeTimeline *tl5;

};

#endif // WIDGET_H
