#ifndef FAKETIMELINE_H
#define FAKETIMELINE_H

#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>

#include <QDebug>


#include "fakepost.h"

class FakeTimeline : public QWidget
{
    Q_OBJECT

public:
    explicit FakeTimeline(QWidget *parent = 0);

signals:

public slots:

private:
    QVBoxLayout *mainLayout;

    QScrollArea *scrollArea;

    QVBoxLayout *containerLayout;
    QFrame *containerFrame;


};

#endif // FAKETIMELINE_H
