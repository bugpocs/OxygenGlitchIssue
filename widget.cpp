#include "widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle("Oxygen style issue test");
    this->setMinimumSize(800, 700);


    tl1 = new FakeTimeline(this);
    tl2 = new FakeTimeline(this);
    tl3 = new FakeTimeline(this);
    tl4 = new FakeTimeline(this);
    tl5 = new FakeTimeline(this);

    tabWidget = new QTabWidget(this);
    tabWidget->addTab(tl1, QIcon::fromTheme("mail-folder-inbox"), "Inbox");
    tabWidget->addTab(tl2, QIcon::fromTheme("mail-message"),      "Messages");
    tabWidget->addTab(tl3, QIcon::fromTheme("user-home"),         "Activity");
    tabWidget->addTab(tl4, QIcon::fromTheme("folder-favorites"),  "Favorites");
    tabWidget->addTab(tl5, QIcon::fromTheme("system-users"),      "Contacts");


    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(tabWidget);
    this->setLayout(mainLayout);

    qDebug() << "Main widget created";
}

Widget::~Widget()
{
    qDebug() << "Main widget destroyed";
}
