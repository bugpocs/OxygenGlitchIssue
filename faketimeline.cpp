#include "faketimeline.h"

FakeTimeline::FakeTimeline(QWidget *parent) : QWidget(parent)
{
    containerLayout = new QVBoxLayout();

    for (int counter=0; counter < 500; ++counter)
    {
        FakePost *fakePost = new FakePost(counter+1, this);
        containerLayout->addWidget(fakePost);
    }

    containerFrame = new QFrame(this);
    containerFrame->setLayout(containerLayout);

    scrollArea = new QScrollArea(this);
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(containerFrame);

    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(scrollArea);
    this->setLayout(mainLayout);
}
