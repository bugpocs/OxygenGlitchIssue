#include "fakepost.h"

FakePost::FakePost(int postNumber, QWidget *parent) : QWidget(parent)
{
    avatarLabel = new QLabel(this);
    avatarLabel->setPixmap(QIcon::fromTheme("user-identity").pixmap(64, 64));
    avatarLabel->setToolTip("this is the user's avatar...");

    nameLabel = new QLabel(QString("User%1").arg(postNumber),
                           this);
    nameLabel->setToolTip("this is the user's name");


    leftFrame = new QFrame(this);
    leftFrame->setObjectName("LeftFrame");
    leftFrame->setStyleSheet("QFrame#LeftFrame"
                             "{ background-color: "
                             "  qlineargradient(spread:pad, "
                             "  x1:0, y1:0, x2:1, y2:0,  "
                             "  stop:0 rgba(0, 0, 0, 0), "
                             "  stop:1 red); "
                             "}");


    contentBrowser = new QTextBrowser(this);
    contentBrowser->setHtml(QString("<h2>Title for %1</h2>"
                                    "Some content, with a "
                                    "<a href=\"http://pump.io\">link</a>..."
                                    "<br /><br />"
                                    "Etc...").arg(postNumber));
    contentBrowser->setMinimumHeight(200);
    contentBrowser->setOpenExternalLinks(true);


    button1 = new QPushButton(QIcon::fromTheme("emblem-favorite"),
                              "Like",
                              this);
    button1->setToolTip("click to like this post");
    connect(button1, SIGNAL(clicked(bool)),
            this, SLOT(like()));

    button2 = new QPushButton(QIcon::fromTheme("mail-reply-sender"),
                              "Comment",
                              this);
    button2->setToolTip("click to post a comment");
    connect(button2, SIGNAL(clicked(bool)),
            this, SLOT(comment()));

    button3 = new QPushButton(QIcon::fromTheme("mail-forward"),
                              "Share",
                              this);
    button3->setToolTip("click to share this post...");
    connect(button3, SIGNAL(clicked(bool)),
            this, SLOT(share()));



    leftLayout = new QVBoxLayout();
    leftLayout->addWidget(avatarLabel);
    leftLayout->addWidget(nameLabel);
    leftLayout->addStretch();

    leftFrame->setLayout(leftLayout);


    buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(button1);
    buttonsLayout->addWidget(button2);
    buttonsLayout->addWidget(button3);


    rightLayout = new QVBoxLayout();
    rightLayout->addWidget(contentBrowser);
    rightLayout->addLayout(buttonsLayout);


    mainLayout = new QHBoxLayout();
    mainLayout->addWidget(leftFrame,   1);
    mainLayout->addLayout(rightLayout, 5);


    updateContentTimer = new QTimer(this);
    connect(updateContentTimer, SIGNAL(timeout()),
            this, SLOT(updateContent()));

    updateContentTimer->start(30000);

    this->setLayout(mainLayout);
}



void FakePost::updateContent()
{
    QString oldHtml = this->contentBrowser->toHtml();
    this->contentBrowser->setHtml(oldHtml + "beep!<br />");
}

void FakePost::like()
{
    QString oldHtml = this->contentBrowser->toHtml();
    this->contentBrowser->setHtml(oldHtml + "<i>Liked!</i><br />");

    this->leftFrame->setStyleSheet("");
}

void FakePost::comment()
{
    QString oldHtml = this->contentBrowser->toHtml();
    this->contentBrowser->setHtml(oldHtml + "<h3>Added a comment!</h3><br />"
                                            "<small>This is a comment...</small>"
                                            "<br />");
}

void FakePost::share()
{
    QString oldHtml = this->contentBrowser->toHtml();
    this->contentBrowser->setHtml(oldHtml + "<u>Shared!</u><br />");
}
